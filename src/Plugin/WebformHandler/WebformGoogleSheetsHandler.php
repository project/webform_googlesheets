<?php

namespace Drupal\webform_googlesheets\Plugin\WebformHandler;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\google_api_client\Entity\GoogleApiClient;
use Drupal\google_api_client\Entity\GoogleApiServiceClient;
use Drupal\webform\Plugin\WebformElement\WebformManagedFileBase;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_googlesheets\Event\WebformGoogleSheetsErrorEvent;
use Drupal\webform_googlesheets\Event\WebformGoogleSheetsSuccessEvent;
use Google\Service\Sheets;
use Google\Service\Sheets\AppendCellsRequest;
use Google\Service\Sheets\AppendDimensionRequest;
use Google\Service\Sheets\BatchUpdateSpreadsheetRequest;
use Google\Service\Sheets\CellData;
use Google\Service\Sheets\ExtendedValue;
use Google\Service\Sheets\Request;
use Google\Service\Sheets\RowData;
use Google\Service\Sheets\Spreadsheet;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Form submission to Google Sheets handler.
 *
 * @WebformHandler(
 *   id = "googlesheets",
 *   label = @Translation("Google Sheets"),
 *   category = @Translation("Google Sheets"),
 *   description = @Translation("Adds a form submission to a Google Sheets."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class WebformGoogleSheetsHandler extends WebformHandlerBase {

  /**
   * A webform element plugin manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $elementManager;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Google API Client.
   *
   * @var \Drupal\google_api_client\Service\GoogleApiClientService
   */
  protected $googleApiClient;

  /**
   * Google API Service Client.
   *
   * @var \Drupal\google_api_client\Service\GoogleApiServiceClientService
   */
  protected $googleApiServiceClient;

  /**
   * Google Sheets service.
   *
   * @var \Google\Service\Sheets
   */
  protected $service;

  /**
   * Entity Field Manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * List of unsupported webform submission properties.
   *
   * The below properties will not being included in a remote post.
   *
   * @var array
   */
  protected $unsupportedProperties = [
    'metatag',
  ];

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The date time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The date format storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $dateFormatStorage;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition, $container->get('event_dispatcher'));

    $instance->loggerFactory = $container->get('logger.factory');
    $instance->configFactory = $container->get('config.factory');
    $instance->renderer = $container->get('renderer');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->conditionsValidator = $container->get('webform_submission.conditions_validator');
    $instance->tokenManager = $container->get('webform.token_manager');

    $instance->elementManager = $container->get('plugin.manager.webform.element');
    $instance->googleApiClient = $container->get('google_api_client.client');
    $instance->googleApiServiceClient = $container->get('google_api_service_client.client');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->time = $container->get('datetime.time');
    $instance->dateFormatStorage = $container->get('entity_type.manager')->getStorage('date_format');

    $instance->setConfiguration($configuration);

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $configuration = $this->getConfiguration();

    return [
        '#settings' => $configuration['settings'],
      ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $field_names = array_keys(
      $this->entityFieldManager->getBaseFieldDefinitions('webform_submission')
    );

    return [
      'spreadsheet_url' => '',
      'spreadsheet_id' => '',
      'spreadsheet_sheet_id' => '',
      'google_api_credential_type' => 'google_api_client',
      'google_api_client' => 0,
      'google_api_service_client' => '',
      'multivalue_limit' => 0,
      'multivalue_empty' => 0,
      'comma_separate' => 0,
      'sort_data' => 0,
      'convert_date' => 1,
      'date_format' => 'default',
      'custom_date_format' => '',
      'included_data' => array_combine($field_names, $field_names),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    parent::buildConfigurationForm($form, $form_state);

    // Google Sheets configuration.
    $form['googlesheets'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Google API settings'),
    ];
    $form['googlesheets']['spreadsheet_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Sheet URL'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['spreadsheet_url'] ?? '',
      '#empty_value' => '',
    ];

    $form['googlesheets']['google_api_credential_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Credential Type'),
      '#required' => TRUE,
      '#options' => [
        'google_api_client' => $this->t('OAuth 2.0 Client'),
        'google_api_service_client' => $this->t('Service Account'),
      ],
      '#default_value' => $this->configuration['google_api_credential_type'] ?? 'google_api_client',
    ];

    $form['googlesheets']['google_api_client'] = [
      '#type' => 'select',
      '#title' => $this->t('OAuth 2.0 Client'),
      '#options' => [],
      '#default_value' => $this->configuration['google_api_client'] ?? 0,
      '#empty_value' => 0,
      '#states' => [
        'visible' => [
          ':input[name="settings[google_api_credential_type]"]' => ['value' => 'google_api_client'],
        ],
        'required' => [
          ':input[name="settings[google_api_credential_type]"]' => ['value' => 'google_api_client'],
        ],
      ],
    ];
    $google_api_clients = GoogleApiClient::loadMultiple();
    foreach ($google_api_clients as $google_api_client) {
      $form['googlesheets']['google_api_client']['#options'][$google_api_client->id()] = $google_api_client->label();
    }

    $form['googlesheets']['google_api_service_client'] = [
      '#type' => 'select',
      '#title' => $this->t('Service Account'),
      '#options' => [],
      '#default_value' => $this->configuration['google_api_service_client'] ?? '',
      '#empty_value' => '',
      '#states' => [
        'visible' => [
          ':input[name="settings[google_api_credential_type]"]' => ['value' => 'google_api_service_client'],
        ],
        'required' => [
          ':input[name="settings[google_api_credential_type]"]' => ['value' => 'google_api_service_client'],
        ],
      ],
    ];
    $google_api_service_clients = GoogleApiServiceClient::loadMultiple();
    foreach ($google_api_service_clients as $google_api_service_client) {
      $form['googlesheets']['google_api_service_client']['#options'][$google_api_service_client->id()] = $google_api_service_client->label();
    }

    // Prepare to check for potential issues.
    $label = '';
    $form['googlesheets']['issues'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [],
    ];
    if ($this->configuration['google_api_credential_type'] == 'google_api_service_client') {
      if (!empty($google_api_service_clients[$this->configuration['google_api_service_client']])) {
        $google_api_service_client = $google_api_service_clients[$this->configuration['google_api_service_client']];
        $scopes = $google_api_service_client->getScopes();
        $services = $google_api_service_client->getServices();
        $label = $google_api_service_client->label();
      }
    }
    else {
      if (!empty($google_api_clients[$this->configuration['google_api_client']])) {
        $google_api_client = $google_api_clients[$this->configuration['google_api_client']];
        $scopes = $google_api_client->getScopes();
        $services = $google_api_client->getServices();
        $label = $google_api_client->label();
      }
    }
    // Check if Google Sheets selected as a service.
    if (isset($services) && !in_array('sheets', $services)) {
      $form['googlesheets']['issues']['#items'][] = $this->t('Select %service as a service.', ['%service' => 'Google Sheets API']);
    }
    // Check if Google Sheets selected as a scope.
    if (isset($scopes) && !in_array('https://www.googleapis.com/auth/spreadsheets', $scopes) && !in_array('SPREADSHEETS', $scopes)) {
      $form['googlesheets']['issues']['#items'][] = $this->t('Select %scope as a scope.', ['%scope' => 'https://www.googleapis.com/auth/spreadsheets']);
    }
    // Check if Google API authenticated.
    if (isset($google_api_client) && !$google_api_client->getAuthenticated()) {
      $link = Link::fromTextAndUrl(
        $this->t('here'),
        Url::fromRoute('google_api_client.callback', [], [
          'attributes' => ['target' => '_blank'],
          'query' => ['id' => $google_api_client->id()],
        ])
      );
      $form['googlesheets']['issues']['#items'][] = $this->t('Perform authentication @link', ['@link' => $link->toString()]);
    }

    // Hide the issues list if there are no issues.
    $form['googlesheets']['issues']['#access'] = !empty($form['googlesheets']['issues']['#items']);
    $form['googlesheets']['issues']['#title'] = !empty($form['googlesheets']['issues']['#items'])
      ? $this->t('%label has following issues:', ['%label' => $label])
      : $this->t('%label is correctly configured', ['%label' => $label]);

    // Settings for Multivalued fields.
    $form['multivalue'] = [
      '#type' => 'details',
      '#title' => $this->t('Multivalue fields'),
    ];
    $form['multivalue']['multivalue_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of values'),
      '#description' => $this->t('Limits the number of values sent to Google Sheets for fields with multiple values. For any field, if this limit is greater than the allowed number of values, the allowed number will be used instead.'),
      '#min' => 0,
      '#max' => 1000,
      '#step' => 1,
      '#default_value' => $this->configuration['multivalue_limit'] ?? 0,
    ];
    $form['multivalue']['multivalue_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include empty'),
      '#description' => $this->t('Fields left empty by the users will be included for multivalued fields, limited by %multivalue_limit setting. This setting helps ensure columns always align on future submissions.', ['%multivalue_limit' => $this->t('Number of values')]),
      '#default_value' => $this->configuration['multivalue_empty'] ?? 0,
    ];
    $form['multivalue']['comma_separate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Comma separate values'),
      '#description' => $this->t('Combine multi-value answers into a single column of comma separated values.'),
      '#default_value' => $this->configuration['comma_separate'] ?? 0,
    ];

    // Timestamp conversion settings.
    $form['timestamp'] = [
      '#type' => 'details',
      '#title' => $this->t('Timestamp conversion'),
    ];
    $form['timestamp']['convert_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Convert timestamps to a date-time format'),
      '#description' => $this->t('Webform submissions include unix timestamps for created, completed, and changed date-times. Check this box to convert these to a date-time format Google Sheets recognizes.'),
      '#default_value' => $this->configuration['convert_date'] ?? 0,
    ];
    // Get site configured date formats.
    $date_formats = [];
    foreach ($this->dateFormatStorage->loadMultiple() as $machine_name => $value) {
      $date_formats[$machine_name] = $this->t('@name: @date', [
        '@name' => $value->label(),
        '@date' => $this->dateFormatter->format(
          $this->time->getRequestTime(), $machine_name
        ),
      ]);
    }
    $date_formats['custom'] = $this->t('Custom');
    $form['timestamp']['date_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#options' => $date_formats,
      '#default_value' => $this->configuration['date_format'] ?? 'default',
    ];
    $form['timestamp']['date_format']['#states']['visible'][] = [
      ':input[name="settings[convert_date]"]' => ['checked' => TRUE],
    ];
    $form['timestamp']['custom_date_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom date format string'),
      '#description' => $this->t('A user-defined date format. See the <a href="https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters">PHP manual</a> for available options.'),
      '#default_value' => $this->configuration['custom_date_format'] ?? '',
    ];
    $form['timestamp']['custom_date_format']['#states']['visible'][] = [
      ':input[name="settings[date_format]"]' => ['value' => 'custom'],
    ];

    // Submission data.
    $form['submission_data'] = [
      '#type' => 'details',
      '#title' => $this->t('Submission data'),
    ];
    $form['submission_data']['sort_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add submission data columns first'),
      '#description' => $this->t('If checked, webform submission data columns (i.e. submission ID, etc.) will appear in the spreadsheet before the data columns submitted by user.'),
      '#default_value' => $this->configuration['sort_data'] ?? 0,
    ];
    $form['submission_data']['included_data'] = [
      '#type' => 'webform_excluded_columns',
      '#title' => $this->t('Submitted data'),
      '#title_display' => 'invisible',
      '#webform_id' => $this->getWebform()->id(),
      '#required' => TRUE,
      '#default_value' => $this->configuration['included_data'] ?? [],
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!$form_state->isValueEmpty('spreadsheet_url')) {
      if (preg_match('#https?://docs.google.com/spreadsheets/d/(?<id>.+?)/.+?(\#gid=(?<sheet_id>\d+))?$#', $form_state->getValue('spreadsheet_url'), $matches)) {
        $form_state->setValue('spreadsheet_id', $matches['id']);
        $form_state->setValue('spreadsheet_sheet_id', $matches['sheet_id'] ?? 0);
      }
      else {
        $form_state->setErrorByName('settings][spreadsheet_url', $this->t('Please provide a valid Google Sheet URL.'));
      }
    }

    if ($form_state->getValue('google_api_credential_type') == 'google_api_client' && $form_state->isValueEmpty('google_api_client')) {
      $form_state->setErrorByName('settings][google_api_client', $this->t('Please select Google API OAuth 2.0 Client.'));
    }
    if ($form_state->getValue('google_api_credential_type') == 'google_api_service_client' && $form_state->isValueEmpty('google_api_service_client')) {
      $form_state->setErrorByName('settings][google_api_service_client', $this->t('Please select Google API Service Account.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // If update operation, do nothing.
    if ($update) {
      return;
    }

    // Fetch the current column count.
    $spreadsheet = $this->handleErrors($webform_submission, function () {
      return $this->getService()->spreadsheets->get($this->configuration['spreadsheet_id']);
    });

    if (!$spreadsheet instanceof Spreadsheet) {
      return;
    }

    $sheet_id = (int) $this->configuration['spreadsheet_sheet_id'];
    $sheets = $spreadsheet->getSheets();

    foreach ($sheets as $sheet) {
      if ($sheet->getProperties()->getSheetId() === $sheet_id) {
        $column_count = $sheet->getProperties()->getGridProperties()->getColumnCount();
        break;
      }
    }

    if (!isset($column_count)) {
      $this->getLogger('webform_submission')->error('Failed to append the submission to Google Sheets: the sheet ID %sheet_id does not exist in the spreadsheet.', ['%sheet_id' => $sheet_id]);
      return;
    }

    // Prepare the update request.
    $data = $this->getData($webform_submission);
    $rows = $this->prepareRows($data);
    $request = $this->prepareRequest($this->configuration['spreadsheet_sheet_id'], $rows, $column_count);

    // Perform the update request.
    $response = $this->handleErrors($webform_submission, function () use ($request) {
      return $this->getService()->spreadsheets->batchUpdate($this->configuration['spreadsheet_id'], $request);
    });

    if ($response !== NULL) {
      $this->getLogger('webform_submission')->notice('Webform submission had been successfully appended to Google Sheets.');
      $this->eventDispatcher->dispatch(new WebformGoogleSheetsSuccessEvent($webform_submission), WebformGoogleSheetsSuccessEvent::EVENT_NAME);
    }
  }

  /**
   * Helper method to prepare webform submission fields.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   A webform submission.
   *
   * @return array<mixed>
   *   An array with fields.
   */
  private function getData(WebformSubmissionInterface $webform_submission) {
    // Get webform and submission element data.
    $data = $webform_submission->toArray(TRUE);
    $webform = $this->getWebform();

    // Remove unsupported properties from data.
    // These are typically added by other module's like metatag.
    $unsupported_properties = array_combine($this->unsupportedProperties, $this->unsupportedProperties);
    $data = array_diff_key($data, $unsupported_properties);

    // Flatten data and sort data order.
    $element_data = $data['data'];
    unset($data['data']);
    $data = $this->configuration['sort_data']
      ? $data + $element_data
      : $element_data + $data;

    // Included selected submission data.
    $data = array_diff_key($data, $this->configuration['included_data']);

    // Get export configuration options.
    $multivalue_limit = $this->configuration['multivalue_limit'];
    $multivalue_empty = $this->configuration['multivalue_empty'];
    $comma_separate = $this->configuration['comma_separate'];
    $convert_date = $this->configuration['convert_date'];
    $date_format = $this->configuration['date_format'];
    $custom_date_format = $this->configuration['custom_date_format']
      ? $this->configuration['custom_date_format']
      : '';
    $date_fields = ['created', 'completed', 'changed'];

    // Replace tokens before re-writing composite and multivalued field keys.
    $data = $this->replaceTokens($data, $webform_submission);

    // Process values, and flatten all composites and multi-valued fields.
    $flat_data = [];
    foreach ($data as $element_key => $value) {
      $element = $webform->getElement($element_key);

      // Webform metadata.
      if (!$element) {
        // Convert timestamps according to user settings.
        if ($convert_date && in_array($element_key, $date_fields)) {
          $value = $this->dateFormatter->format($value, $date_format, $custom_date_format);
        }
        $flat_data[$element_key] = $value;
        continue;
      }

      // Process single-valued fields and continue.
      if (!is_array($value)) {
        $flat_data[$element_key] = $this->processValue($element, $value);
        continue;
      }

      // Determine how many multi-values to include
      // by casting #webform_multiple to integer.
      $multiple = empty($element['#webform_multiple']) ? 0 : (int) $element['#webform_multiple'];
      // Change unlimited from 1 to 9999 for ease of logic.
      $multiple = ($multiple == 1) ? 9999 : $multiple;

      // Single-valued and unlimited custom composites
      // both return true for #webform_multiple.
      // Single-valued should return 0 like other element types,
      // but for now identify them with #multiple.
      // Only single-valued composites have a #multiple value of false,
      // unlimited have #multiple undefined.
      if (($element['#type'] == 'webform_custom_composite') && isset($element['#multiple']) && ($element['#multiple'] == 0)) {
        $multiple = 0;
      }
      // Likert fields identify as multiple = false.
      if ($element['#type'] == 'webform_likert') {
        $multiple = '9999';
      }

      // Set $max columns to the lesser of the max allowed values,
      // or the limit configured on the handler.
      $max = ($multiple > $multivalue_limit) ? $multivalue_limit : $multiple;

      // Process composites that aren't likert.
      if ($element['#webform_composite'] && $element['#type'] !== 'webform_likert') {
        $fields = $element['#webform_composite_elements'];
        // Process a single instance composite.
        if ($multiple == 0) {
          foreach ($fields as $field => $composite_element) {
            if (isset($data[$element_key][$field])) {
              $flat_data[$element_key . '_' . $field] = $this->processValue($composite_element, $value[$field]);
            }
            // Custom composites values are nested one level deeper.
            elseif (isset($data[$element_key][0][$field])) {
              $flat_data[$element_key . '_' . $field] = $this->processValue($composite_element, $value[0][$field]);
            }
            else {
              $flat_data[$element_key . '_' . $field] = NULL;
            }
          }
        }
        // Process composites with multiple values.
        else {
          // User submitted composites up to the multivalue limit set by user.
          $size = (count($value) > $multivalue_limit) ? $multivalue_limit : count($value);
          for ($i = 0; $i < $size; $i++) {
            foreach ($fields as $field => $composite_element) {
              if (isset($data[$element_key][$i][$field])) {
                $flat_data[$element_key . '_' . $field . '_' . $i] = $this->processValue($composite_element, $value[$i][$field]);
              }
              else {
                $flat_data[$element_key . '_' . $field . '_' . $i] = NULL;
              }
            }
          }
          // Add empty composites up the limit.
          if ($multivalue_empty) {
            for ($size; $size < $max; $size++) {
              foreach ($fields as $field => $composite_element) {
                $flat_data[$element_key . '_' . $field . '_' . $size] = NULL;
              }
            }
          }
        }
        continue;
      }

      // Process likert fields.
      if ($element['#type'] == 'webform_likert') {
        if ($comma_separate) {
          $flatdata[$element_key] = '';
          foreach ($data[$element_key] as $key => $answer) {
            $flatdata[$element_key] .= $this->processValue($element, $value[$key]) . ',';
          }
          $flatdata[$element_key] = rtrim($flatdata[$element_key], ',');
        }
        else {
          foreach ($data[$element_key] as $key => $answer) {
            $flatdata[$element_key . '_' . $key] = $this->processValue($element, $value[$key]);
          }
        }
      }
      // Process multivalued fields that are not composites.
      if ($element['#webform_multiple']) {
        if ($comma_separate) {
          $flat_data[$element_key] = '';
          for ($i = 0; $i < $max; $i++) {
            if (isset($data[$element_key][$i])) {
              $flat_data[$element_key] .= $this->processValue($element, $value[$i]) . ',';
            }
          }
          $flat_data[$element_key] = rtrim($flat_data[$element_key], ',');
        }
        else {
          for ($i = 0; $i < $max; $i++) {
            if (isset($data[$element_key][$i])) {
              $flat_data[$element_key . '_' . $i] = $this->processValue($element, $value[$i]);
            }
            elseif ($multivalue_empty) {
              $flat_data[$element_key . '_' . $i] = NULL;
            }
          }
        }
      }
    }

    // Return values only.
    return array_values($flat_data);
  }

  /**
   * Process a single value.
   *
   * @param array<mixed> $element
   *   An element.
   * @param mixed $value
   *   The element's value.
   *
   * @return mixed
   *   The element's value cast to formats ready for Google Sheets
   */
  protected function processValue(array $element, $value) {
    // Ignore empty and not equal to zero values.
    // @see https://stackoverflow.com/questions/732979/php-whats-an-alternative-to-empty-where-string-0-is-not-treated-as-empty
    if (empty($value) && $value !== 0 && $value !== '0') {
      return $value;
    }

    // Cast markup to string. This only applies to computed Twig values.
    // @see \Drupal\webform\Element\WebformComputedTwig::computeValue
    if ($value instanceof MarkupInterface) {
      return $value = (string) $value;
    }

    $plugin = $this->elementManager->getElementInstance($element);

    // For files send the URI.
    if ($plugin instanceof WebformManagedFileBase) {
      /** @var \Drupal\file\FileInterface $file */
      $file = File::load($value);
      if ($file) {
        // Potential TODO:
        // User option to instead return file name with $file->getFilename()
        $value = $file->getFileUri();
      }
    }

    return $value;
  }

  /**
   * Helper method to prepare cell object.
   *
   * @param string $data
   *   A string to insert into a cell.
   *
   * @return \Google\Service\Sheets\CellData
   *   Cells to append in the row.
   */
  private function prepareCell($data) {
    $value = new ExtendedValue();

    switch (gettype($data)) {
      case 'boolean':
        $value->setBoolValue($data);
        break;

      case 'integer':
      case 'double':
        $value->setNumberValue($data);
        break;

      case 'string':
        $value->setStringValue($data);
        break;

      default:
        // Set nothing?
        break;
    }

    $cell = new CellData();
    $cell->setUserEnteredValue($value);

    return $cell;
  }

  /**
   * Helper method to prepare row object.
   *
   * @param array<mixed> $data
   *   An array with row values.
   *
   * @return \Google\Service\Sheets\RowData
   *   Rows to append in the request.
   */
  private function prepareRows(array $data) {
    $row = new RowData();
    $row->setValues(array_map([$this, 'prepareCell'], array_values($data)));

    return $row;
  }

  /**
   * Helper method to prepare Google API request.
   *
   * @param string $sheet_id
   *   A string with Google Spreadsheet Sheet ID.
   * @param \Google\Service\Sheets\RowData $row
   *   Rows to append in the request.
   * @param int $column_count
   *   Number of columns in the sheet.
   *
   * @return \Google\Service\Sheets\BatchUpdateSpreadsheetRequest
   *   Batch update request for a spreadsheet.
   */
  private function prepareRequest(string $sheet_id, RowData $row, int $column_count) {
    $requests = [];

    if ($row->count() > $column_count) {
      $append_dimension_request = new AppendDimensionRequest();
      $append_dimension_request->setSheetId($sheet_id);
      $append_dimension_request->setDimension('COLUMNS');
      $append_dimension_request->setLength($row->count() - $column_count);

      $request = new Request();
      $request->setAppendDimension($append_dimension_request);
      $requests[] = $request;
    }

    $append_cells_request = new AppendCellsRequest();
    $append_cells_request->setSheetId($sheet_id);
    $append_cells_request->setRows($row);
    $append_cells_request->setFields('userEnteredValue');

    $request = new Request();
    $request->setAppendCells($append_cells_request);
    $requests[] = $request;

    return new BatchUpdateSpreadsheetRequest(['requests' => $requests]);
  }

  /**
   * Helper method to perform Google API request.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   A webform submission.
   * @param callable $callback
   *   A callable function that performs the request.
   *
   * @return mixed
   *   The response from the Google API, or NULL if an error occurred.
   */
  private function handleErrors(WebformSubmissionInterface $webform_submission, callable $callback) {
    $response = NULL;
    $error = NULL;

    try {
      $response = $callback();
    }
    catch (\Exception $e) {
      $error = $e->getMessage();
    }

    // Log detailed error message to the 'webform_submission' log.
    if ($error !== NULL) {
      $this->eventDispatcher->dispatch(new WebformGoogleSheetsErrorEvent($webform_submission, "error occurred while appending submission to Google Sheets."), WebformGoogleSheetsErrorEvent::EVENT_NAME);
      $this->getLogger('webform_submission')->error('%error error occurred while appending submission to Google Sheets.', [
        'link' => ($webform_submission->id()) ? $webform_submission->toLink($this->t('View'))->toString() : NULL,
        'webform_submission' => $webform_submission,
        'handler_id' => $this->getHandlerId(),
        'operation' => 'google sheets append',
        '%error' => $error,
      ]);
    }

    return $response;
  }

  /**
   * Get the Google Sheets service.
   *
   * @return \Google\Service\Sheets
   *   Google Sheets service.
   */
  private function getService(): Sheets {
    if (isset($this->service)) {
      return $this->service;
    }

    // Try to load the client.
    if ($this->configuration['google_api_credential_type'] == 'google_api_service_client') {
      $google_api_service_client = GoogleApiServiceClient::load($this->configuration['google_api_service_client'] ?? '');
    }
    else {
      $google_api_client = GoogleApiClient::load($this->configuration['google_api_client'] ?? 0);
    }

    // Get the service based on what type of credentials we are using.
    if (!empty($google_api_service_client)) {
      $this->googleApiServiceClient->setGoogleApiClient($google_api_service_client);
      $this->service = new Sheets($this->googleApiServiceClient->googleClient);
    }
    elseif (!empty($google_api_client)) {
      $this->googleApiClient->setGoogleApiClient($google_api_client);
      $this->service = new Sheets($this->googleApiClient->googleClient);
    }
    else {
      throw new \Exception("Unable to find Google API Entity with type " . $this->configuration['google_api_credential_type'] . " and ID " . $this->configuration[$this->configuration['google_api_credential_type']], 1);
    }

    return $this->service;
  }

}
