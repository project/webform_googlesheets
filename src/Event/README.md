# Webform Google Sheets Events

This module provides events that can be listened to when a webform submission is sent to Google Sheets.

## Subscribing to Events

You can subscribe to these events in your event subscriber:

```php
public static function getSubscribedEvents(): array
{
    return [
        WebformGoogleSheetsSuccessEvent::EVENT_NAME => 'onSuccess',
        WebformGoogleSheetsErrorEvent::EVENT_NAME => 'onError',
    ];
}
```

Use this in an `EventSubscriber` to handle success or error cases accordingly.