<?php

namespace Drupal\webform_googlesheets\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Event triggered when sending to Google Sheets fails.
 */
class WebformGoogleSheetsErrorEvent extends Event
{
  const EVENT_NAME = 'webform_googlesheets.error';
  /**
   * The related Webform submission.
   *
   * @var WebformSubmissionInterface
   */
  protected WebformSubmissionInterface $submission;
  /**
   * The error message.
   *
   * @var string
   */
  protected string $errorMessage;
  /**
   * Event constructor.
   */
  public function __construct(WebformSubmissionInterface $submission, $errorMessage)
  {
    $this->submission   = $submission;
    $this->errorMessage = $errorMessage;
  }
  /**
   * Retrieves the Webform submission.
   */
  public function getSubmission()
  {
    return $this->submission;
  }
  /**
   * Retrieves the error message.
   */
  public function getErrorMessage()
  {
    return $this->errorMessage;
  }
}
