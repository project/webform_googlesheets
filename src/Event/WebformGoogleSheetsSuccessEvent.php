<?php

namespace Drupal\webform_googlesheets\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Event triggered when sending to Google Sheets is successful.
 */
class WebformGoogleSheetsSuccessEvent extends Event
{
  const EVENT_NAME = 'webform_googlesheets.success';
  /**
   * The related Webform submission.
   *
   * @var WebformSubmissionInterface
   */
  protected WebformSubmissionInterface $submission;
  /**
   * Event constructor.
   */
  public function __construct(WebformSubmissionInterface $submission)
  {
    $this->submission = $submission;
  }
  /**
   * Retrieves the Webform submission.
   */
  public function getSubmission(): WebformSubmissionInterface
  {
    return $this->submission;
  }
}
